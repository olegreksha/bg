//
//  PlayerCell.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import UIKit

class PlayerCell: UITableViewCell {

    @IBOutlet weak var playerName: UILabel!
    @IBOutlet weak var playerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       playerImage.layer.cornerRadius = 20
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
       // super.setSelected(selected, animated: animated)
        
        if selected {
            playerImage.layer.borderColor = UIColor.cyan.cgColor
            playerImage.layer.borderWidth = 2
        } else{
            playerImage.layer.borderWidth = 0
        }
        
    }

}
