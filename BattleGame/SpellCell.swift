//
//  SpellCell.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import UIKit

class SpellCell: UITableViewCell {

    @IBOutlet weak var spellIcon: UIImageView!
    @IBOutlet weak var spellNameLabel: UILabel!
    @IBOutlet weak var spellDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
       // super.setSelected(selected, animated: animated)
        if selected {
            spellIcon.layer.borderColor = UIColor.red.cgColor
            spellIcon.layer.borderWidth = 2
        } else {
            spellIcon.layer.borderWidth = 0
        }
        
    }

}
