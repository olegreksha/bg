//
//  FireBall.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SpriteKit

class FireBall: Spell {
    
    override init() {
        super.init()
        self.cost = 5
        self.damage = 20
        self.icon = "fireball"
        self.sprite = "fireball"
        self.hitSprite = "FireExplosion"
        self.name = "FireBall"
        self.type = .Fire
    }
    
    override func cast(player: Player, enenmy: Player, scene: SKScene) {
        if player.MP < cost {
            return
        }
        let shSprite = SKSpriteNode(imageNamed: sprite!)
        shSprite.position = (player.shutNode?.position)!
        shSprite.size = CGSize(width: 150, height: 150)
        shSprite.zPosition = 4
        player.MP -= cost
        shSprite.alpha = 0.6
        //shSprite.scale(to: CGSize(width: 2, height: 2))
        shSprite.physicsBody = SKPhysicsBody(circleOfRadius: 25)
        shSprite.name = "spell"
        scene.addChild(shSprite)
        shSprite.run(SKAction.scale(by: 1.2, duration: 0.3))
        shSprite.run(SKAction.rotate(toAngle: 360.0, duration: 0.5))
        let dx = player.isPlayer ? 400 : -400
        shSprite.physicsBody?.applyImpulse(CGVector(dx: dx, dy: 0))
    
    }
    override func hit(_ target: Player) {
        let sprite = SKSpriteNode(fileNamed: hitSprite!)
        sprite?.zPosition = 10
        sprite?.position.y -= (target.node?.size.width)! / 2
        var actionArray = [SKAction]()
        actionArray.append(SKAction.customAction(withDuration: 1, actionBlock: { (node, flt) in
            sprite?.removeFromParent()
            target.node?.addChild(sprite!)
        }))
        actionArray.append(SKAction.customAction(withDuration: 0.1, actionBlock: { (_, _) in
            sprite?.removeFromParent()
            
        }))
      
        target.node?.run(SKAction.sequence(actionArray)){
           if target.isPlayer {
            target.HP -= self.damage
        }
        }
    }
    
}
