//
//  UserInfoViewController.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import UIKit

class UserInfoViewController: UIViewController, UITextFieldDelegate {
    let charactersArray = ["naruto", "jira","pein", "itachi"]
    var selectedChar:Int = 0
    
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var nicknameLabel: UITextField!
    @IBOutlet weak var charactersScrollView: UIScrollView!
    @IBOutlet weak var nextButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        nicknameLabel.delegate = self
        nicknameLabel.text = defaults.string(forKey: "nick")
        selectedChar = defaults.integer(forKey: "char")
        setupChars()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func setupChars(){
        charactersScrollView.contentSize.width = CGFloat(charactersArray.count * 205)
        charactersScrollView.subviews.forEach({$0.removeFromSuperview()})
        var i = 0
        for item in charactersArray {
            let imageView = UIImageView(image: UIImage(named: item))
            imageView.frame.size = CGSize(width: 200, height: 200)
            imageView.frame.origin.x = CGFloat(200 * i + 5)
            imageView.layer.cornerRadius = 10
            charactersScrollView.addSubview(imageView)
            imageView.tag = i
            imageView.isUserInteractionEnabled = true
            imageView.contentMode = .scaleAspectFit
            imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(charSelected(_:))))
            if i == selectedChar{
                imageView.layer.borderWidth = 2
                imageView.layer.borderColor = UIColor.cyan.cgColor
                
            }
            i += 1
            
        }
    }

    func charSelected(_ sender: UITapGestureRecognizer){
        let view  = sender.view as! UIImageView
        selectedChar = view.tag
        setupChars()
    }
    
    
    @IBAction func startClick(_ sender: UIButton) {
        defaults.set(nicknameLabel.text, forKey: "nick")
        defaults.set(selectedChar, forKey: "char")
        
        playerSettings.name = nicknameLabel.text!
        playerSettings.texture = charactersArray[selectedChar]
     
        
    }
}
