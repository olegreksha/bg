//
//  PickBattleViewController.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import UIKit

class PickBattleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var playersList = [Player]()
    override func viewDidLoad() {
        super.viewDidLoad()
        socket.disconnect()
        socket.connect()
        
        socket.on(clientEvent: .connect) { (data, sae) in
            var data = Dictionary<String, Any>()
            playerSettings.id = socket.sid
            data["sid"] = socket.sid
            data["player_name"] = playerSettings.name
            data["player_actor"] = playerSettings.texture
            data["wins"] = playerSettings.wins
            data["loses"] = playerSettings.loses
            
            socket.emit("init", data)
        }
        
        socket.on("usersList") { (data, _) in
            let result = data.first as! Dictionary<String, Any>
            self.playersList.removeAll()
            for (_, v) in result {
                let value = v as! Dictionary<String, Any>
                let player = Player()
                player.texture = value["player_actor"] as? String
                player.name = value["player_name"] as? String
                player.id = value["sid"] as? String
                player.loses = (value["loses"] as? Int)!
                player.wins = (value["wins"] as? Int)!
                self.playersList.append(player)
            }
            self.tableView.reloadData()
            print(result)
        }
        
        socket.on("requestFight") { (data, _) in
            
            print(data)
            let request = data.first as! Dictionary<String, String>
            
            let alert = UIAlertController(title: "New challenge from \(request["name"] ?? "unknown")", message: "Accept or reject?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Accept", style: UIAlertActionStyle.default) { _ in
                var data = Dictionary<String, Any>()
                data["to_id"] = request["id"]
                data["response"] = true
                socket.emit("responseFight", data)
                self.acceptBattle(id: request["id"]!)
                playersMove = true
            })
            alert.addAction(UIAlertAction(title: "Reject", style: .destructive, handler: { _ in
                var data = Dictionary<String, Any>()
                data["to_id"] = request["id"]
                data["response"] = false
                 playersMove = false
                socket.emit("responseFight", data)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        socket.on("responseFight") { (data, _) in
            if data.first as! Bool {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "choseSpellVC") as! ChooseSpellViewController
                self.present(vc, animated: true, completion: nil)
            } else {
             
            }
        }
        
    }
    
    func acceptBattle(id: String){
        currentEnemy = playersList.first(where: {$0.id! == id})!
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "choseSpellVC") as! ChooseSpellViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentEnemy = playersList[indexPath.row]
        
        var data = Dictionary<String,String>()
        data["to_id"] = currentEnemy.id
        data["id"] = playerSettings.id
        data["name"] = playerSettings.name
        socket.emit("requestFight", data)
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playersList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playerCell") as! PlayerCell
        let item = playersList[indexPath.row]
        cell.playerImage.image = UIImage(named: item.texture!)
        cell.playerName.text = "\(item.name!) Lose: \(item.loses) Wins: \(item.wins)"
        return cell
        
    }
    
   
    
}
