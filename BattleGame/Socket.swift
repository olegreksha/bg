//
//  Socket.swift
//  BattleGame
//
//  Created by Олег Рекша on 07.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SocketIO

let socket = SocketIOClient(socketURL: URL(string: "https://magicduelgame.herokuapp.com")!, config: [.log(true), .compress])
