//
//  Amaterasu.swift
//  BattleGame
//
//  Created by Олег Рекша on 07.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SpriteKit

class Amaterasu : Spell {
    
    override init() {
        super.init()
        damage = 30
        cost = 10
        self.icon = "sharingan"
        self.sprite = "sharingan"
        self.hitSprite = "BlackFire"
        self.name = "Amaterasu"
        self.type = .Fire
    }
    
    override func cast(player: Player, enenmy: Player, scene: SKScene) {
        
        if player.MP < cost {
            return
        }
        
        let shSprite = SKSpriteNode(imageNamed: sprite!)
        shSprite.position = (enenmy.node?.position)!
        shSprite.zPosition = 4
        player.MP -= cost
        player.HP -= cost
        shSprite.alpha = 0.1
       // shSprite.scale(to: CGSize(width: 0.9, height: 0.9))
         scene.addChild(shSprite)
        var actionArray = [SKAction]()
        actionArray.append(SKAction.fadeAlpha(to: 0.3, duration: 0.5))
        actionArray.append(SKAction.rotate(byAngle: 360, duration: 1))
        actionArray.append(SKAction.scale(by: 0.1, duration: 0.5))
        actionArray.append(SKAction.fadeAlpha(to: 0.1, duration: 0.5))
        actionArray.append(SKAction.customAction(withDuration: 0, actionBlock: { (node, flt) in
            shSprite.removeFromParent()
           
        }))
        shSprite.run(SKAction.sequence(actionArray)) {
             self.hit(enenmy)
        }
        
    }
    
    override func hit(_ target: Player) {
        playersMove = target.isPlayer
        let sprite = SKSpriteNode(fileNamed: "BlackFire")
        sprite?.zPosition = 10
        sprite?.position.y -= (target.node?.size.width)! / 2
        var actionArray = [SKAction]()
        actionArray.append(SKAction.customAction(withDuration: 5, actionBlock: { (node, flt) in
            sprite?.removeFromParent()
            target.node?.addChild(sprite!)
        }))
        actionArray.append(SKAction.customAction(withDuration: 0.1, actionBlock: { (_, _) in
            sprite?.removeFromParent()
            
        }))
              target.node?.run(SKAction.sequence(actionArray)){
                if target.isPlayer {
            target.HP -= self.damage
        }
        }
    }
    
}
