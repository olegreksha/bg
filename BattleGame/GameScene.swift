//
//  GameScene.swift
//  BattleGame
//
//  Created by Олег Рекша on 07.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var vc:UIViewController?
    var currenrSpell:Spell?
    var spells = [RockSpell(), Amaterasu(), Chidori(), FireBall()]
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
        playerSettings.node = childNode(withName: "player") as? SKSpriteNode
        playerSettings.shutNode = childNode(withName: "playerShotNode")
        playerSettings.isPlayer = true

        currentEnemy.node = childNode(withName: "enemy") as? SKSpriteNode
        currentEnemy.shutNode = childNode(withName: "enemyShutNode")
       fillSpells()
        emitsListeners()
       
    }
    
    
    func emitsListeners(){
        socket.on("attack") { (data, sae) in
            self.enemyCast(spellName: data.first as! String)
        }
        socket.on("HPchanged") { (data, _) in
            var response = data.first as! Dictionary<String, Any>
            currentEnemy.HP = response["hp"] as! Int
        }
        socket.on("dead") { (data, _) in
             var response = data.first as! Dictionary<String, Any>
            var title = ""
            if response["id"] as? String == playerSettings.id{
                title = "You lose!"
                playerSettings.loose()
            } else {
                title = "You win!"
                playerSettings.win()
            }
            let alert = UIAlertController(title: title, message: "Go to Choose battle screen?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
                let startVC = self.vc?.storyboard?.instantiateViewController(withIdentifier: "pickBattleVC")
                self.vc?.present(startVC!, animated: true, completion: nil)
            }))
            alert.addAction(UIAlertAction(title: "Go to Start", style: .destructive, handler: { _ in
                let startVC = self.vc?.storyboard?.instantiateViewController(withIdentifier: "startVC")
                self.vc?.present(startVC!, animated: true, completion: nil)
            }))
            
            self.vc?.present(alert, animated: true, completion: nil)
            
        }
    }
    
   
    
    func enemyCast(spellName:String){
        currenrSpell = allSpells.first(where:{ $0.name == spellName})
        currenrSpell?.cast(player: currentEnemy, enenmy: playerSettings, scene: self)
       
       
    }
    
    func fillSpells(){
        let holder = childNode(withName: "spellHolder") as? SKSpriteNode
        let size: CGFloat =  10
        var i:CGFloat = 0
       // let start =  (holder?.size.width)! / 2
        for item in spells{
            let spell = SKSpriteNode(texture: SKTexture(imageNamed: item.icon!))
            spell.position.x = i * size
            spell.position.y = 2
            spell.scale(to: CGSize(width: 10, height: 100))
            spell.name = item.name
           
            holder?.addChild(spell)
            i += 1
        }
    
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         let touch:UITouch = touches.first! as UITouch
        let positionInScene = touch.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        if !playersMove {return}
        if let name = touchedNode.name{
           currenrSpell =  spells.first(where: {$0.name == name})
            if currenrSpell != nil{
                var data = Dictionary<String, String>()
                data["to"] = currentEnemy.id
                data["attack_type"] = currenrSpell?.name
                socket.emit("attack", data)
            currenrSpell?.cast(player: playerSettings, enenmy: currentEnemy, scene: self)
            }
        }

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        let touch:UITouch = touches.first! as UITouch
//        let positionInScene = touch.location(in: self)
//        let touchedNode = self.atPoint(positionInScene)
//        
//        if touchedNode.name != nil{
//            if currenrSpell?.name == "Shield" {
//                (currenrSpell as! ShieldSpell).stopCast()
//            }
//        }
   
    }
    
    
    func didBegin(_ contact: SKPhysicsContact) {
//       print("Body A: \(contact.bodyA.node?.name!) Body b: \(contact.bodyB.node?.name!)")
        if contact.bodyA.node?.name == "enemy" && contact.bodyB.node?.name == "spell"{
            contact.bodyB.node?.removeFromParent()
            currenrSpell?.hit(currentEnemy)
            playersMove = false
            
        }
        
        if contact.bodyA.node?.name == "player" && contact.bodyB.node?.name == "spell"{
             contact.bodyB.node?.removeFromParent()
            currenrSpell?.hit(playerSettings)
            playersMove = true
           
        }
        
        if contact.bodyA.node?.name == "wall" && contact.bodyB.node?.name == "spell" {
            contact.bodyB.node?.removeFromParent()
        }
        if contact.bodyA.node?.name == "spell" && contact.bodyB.node?.name == "wall"{
            contact.bodyA.node?.removeFromParent()
        }
        if contact.bodyA.node?.name == "spell" && contact.bodyB.node?.name == "shield" {
            contact.bodyA.node?.removeFromParent()
            contact.bodyB.node?.removeFromParent()

        }
        if contact.bodyB.node?.name == "spell" && contact.bodyA.node?.name == "shield" {
            contact.bodyA.node?.removeFromParent()
              contact.bodyB.node?.removeFromParent()
        }
    
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
