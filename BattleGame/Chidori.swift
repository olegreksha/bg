//
//  Chidori.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SpriteKit

class Chidori : Spell {
    
    override init() {
        super.init()
        self.name = "Chidori"
        self.cost = 5
        self.damage = 20
        self.icon = "lightning"
        self.sprite = "ElectroBall"
        self.hitSprite = "ElectricDamage"
        self.type = .Electric
    }
    
    override func cast(player: Player, enenmy: Player, scene: SKScene) {
        if player.MP < cost {
            return
        }
        
        let spell = SKSpriteNode(fileNamed: sprite!)
        
        spell?.size = CGSize(width: 100, height: 100)
        spell?.zPosition = 2
        spell?.name = "spell"
        spell?.physicsBody = SKPhysicsBody(circleOfRadius: 25)
        spell?.physicsBody?.collisionBitMask = 2
        spell?.physicsBody?.contactTestBitMask = 1
        spell?.position = (player.shutNode?.position)!
        scene.addChild(spell!)
        let dx = player.isPlayer ? 300 : -300
        spell?.physicsBody?.applyImpulse(CGVector(dx: dx, dy: 0))
        player.MP -= cost
    }
    
    override func hit(_ target: Player) {
        let effect = SKSpriteNode(fileNamed: hitSprite!)
        if target.isPlayer {
        target.HP -= damage
        }
        var actionArray = [SKAction]()
        actionArray.append(SKAction.customAction(withDuration: 0, actionBlock: { (node, float) in
            effect?.removeFromParent()
            target.node?.addChild(effect!)
        }))
        actionArray.append(SKAction.wait(forDuration: 5))
        actionArray.append(SKAction.customAction(withDuration: 0, actionBlock: { (node, float) in
            effect?.removeFromParent()
        }))
        target.node?.run(SKAction.sequence(actionArray))
    }

    
}
