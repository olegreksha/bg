//
//  Player.swift
//  BattleGame
//
//  Created by Олег Рекша on 07.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SpriteKit

class Player{
    var name:String?
    var id:String?
    var texture:String?
    var mpLabel:SKLabelNode?
    var hpLabel:SKLabelNode?
    var isPlayer:Bool = false
    var shutNode:SKNode?
    var wins: Int = 0
    var loses: Int = 0
    
    var node:SKSpriteNode?{
        didSet{
           self.hpLabel = node?.childNode(withName: "HPLabel") as? SKLabelNode
            self.mpLabel = node?.childNode(withName: "MPLabel") as? SKLabelNode
            node?.texture = SKTexture(imageNamed: texture!)
        }
    }
    
    init() {
        
    }
    
    init(_ player: Bool) {
        isPlayer = player
        if isPlayer {
            wins = UserDefaults.standard.integer(forKey: "wins")
            loses = UserDefaults.standard.integer(forKey: "loses")
        }
    }
   
    
    func loose(){
        loses += 1
        UserDefaults.standard.set(loses, forKey: "loses")
    }
    
    func win(){
        wins += 1
        UserDefaults.standard.set(wins, forKey: "wins")
    }
  
    
    var MP = 100{
        didSet{
            mpLabel?.text = "MP: \(MP)"
        }
    }
    
    var HP = 100{
        didSet{
            hpLabel?.text = "HP: \(HP)"
            if isPlayer {
                var data = Dictionary<String, Any>()
                data["id"] = self.id
                data["hp"] = HP
                data["to_id"] = currentEnemy.id
                socket.emit("HPchanged", data)

            }
            
            if HP <= 0{
                node?.texture = SKTexture(imageNamed: "grave")
                
                if (node?.xScale)! < CGFloat(0) {
                    node?.xScale *= -1
                }
                node?.physicsBody?.pinned = true
                hpLabel?.removeFromParent()
                mpLabel?.removeFromParent()
                var data = Dictionary<String, Any>()
                data["id"] = self.id
                data["hp"] = HP
                data["to_id"] = currentEnemy.id
                socket.emit("dead", data)
            }
        }
    }
}
