//
//  ShieldSpell.swift
//  BattleGame
//
//  Created by Олег Рекша on 11.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SpriteKit

class ShieldSpell :Spell {
    var spell:SKSpriteNode?
    override init() {
        super.init()
        self.cost = 5
        self.damage = 0
        self.icon = "shield"
        self.sprite = "buble"
        self.hitSprite = ""
        self.name = "Shield"
    }
 
    override func cast(player: Player, enenmy: Player, scene: SKScene) {
        spell = SKSpriteNode(imageNamed: sprite!)
        spell?.physicsBody = SKPhysicsBody(circleOfRadius: 110)
        spell?.physicsBody?.pinned = true
        spell?.name = "shield"
        spell?.texture = SKTexture(imageNamed: self.sprite!)
        spell?.size = CGSize(width: 220, height: 220)
        player.node?.addChild(spell!)
           }
    
    func stopCast(){
        spell?.removeAction(forKey: self.name!)
        spell?.removeFromParent()
        
    }
    
    

}
