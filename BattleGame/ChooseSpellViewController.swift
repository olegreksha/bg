//
//  ChooseSpellViewController.swift
//  BattleGame
//
//  Created by Олег Рекша on 08.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import UIKit

class ChooseSpellViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var meReady = false
    var enemyReady = false
    
    var spells = allSpells
    var checkedSpells = [Spell]()
    override func viewDidLoad() {
        super.viewDidLoad()
        socket.on("playerReady") { (data, _) in
            var response = data.first as! Dictionary<String, Any>
            self.enemyReady = response["ready"] as! Bool
            if self.meReady && self.enemyReady {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "battleScene") as! GameViewController
                vc.spells = self.checkedSpells
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let spell = spells[indexPath.row]
        checkedSpells.append(spell)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let spell = spells[indexPath.row]
        let index = checkedSpells.index(where: {$0.name == spell.name})
        checkedSpells.remove(at: index!)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return spells.count 
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "spellCell") as! SpellCell
        let spell = spells[indexPath.row]
        cell.spellIcon.image =  UIImage(named: spell.icon!)
        cell.spellNameLabel.text = spell.name
        cell.spellDescriptionLabel.text = "Cost: \(spell.cost)  Damage: \(spell.damage)"
        return cell
        
    }

    
    @IBAction func readyClick(_ sender: UIButton) {
               meReady = true
        var data = Dictionary<String, Any>()
        data["to_id"] = currentEnemy.id
        data["id"] = playerSettings.id
        data["ready"] = true
        socket.emit("playerReady", data)
        if self.meReady && self.enemyReady {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "battleScene") as! GameViewController
            vc.spells = self.checkedSpells
            self.present(vc, animated: true, completion: nil)
        }


    }

   

}
