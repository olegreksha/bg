//
//  RockSpell.swift
//  BattleGame
//
//  Created by Олег Рекша on 07.09.17.
//  Copyright © 2017 Олег Рекша. All rights reserved.
//

import Foundation
import SpriteKit
class RockSpell: Spell {

    override init() {
        super.init()
        self.cost = 5
        self.damage = 20
        self.icon = "rock"
        self.sprite = "rock"
        self.hitSprite = "HitRock"
        self.name = "Sky Rock"
        self.type = .Rock
    }
    
    override func cast(player: Player, enenmy: Player, scene: SKScene) {
        if player.MP < cost {
            return
        }
        
        let spell = SKSpriteNode(imageNamed: sprite!)
        
        spell.size = CGSize(width: 100, height: 100)
        spell.zPosition = 2
        spell.name = "spell"
        spell.physicsBody = SKPhysicsBody(circleOfRadius: 25)
        spell.physicsBody?.collisionBitMask = 2
        spell.physicsBody?.contactTestBitMask = 1
        spell.position = (player.shutNode?.position)!
        scene.addChild(spell)
        let dx = player.isPlayer ? 500 : -500
        spell.physicsBody?.applyImpulse(CGVector(dx: dx, dy: 0))
        player.MP -= cost
    }
    
    override func hit(_ target: Player) {
        let effect = SKSpriteNode(fileNamed: hitSprite!)
        if target.isPlayer {
        target.HP -= damage
        }
        var actionArray = [SKAction]()
        actionArray.append(SKAction.customAction(withDuration: 0.5, actionBlock: { (node, float) in
            effect?.removeFromParent()
            target.node?.addChild(effect!)
        }))
        actionArray.append(SKAction.customAction(withDuration: 0.5, actionBlock: { (node, float) in
            effect?.removeFromParent()
        }))
        target.node?.run(SKAction.sequence(actionArray))
    }
    
    
}
